﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalWebServer.Models;
using DentalWebServer.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore.Internal;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgramariController : ControllerBase
    {
        private readonly DentalWebContext _context;
        private Repository<Programare> _programari;
        private Repository<Medic> _medics;

        public ProgramariController(DentalWebContext context)
        {
            _context = context;
            this._programari = new Repository<Programare>(this._context);
            this._medics = new Repository<Medic>(this._context);
        }

        // GET: api/Programari
        [HttpGet]
        [Authorize(Roles = "Medic, Client")]
        [Route("api/Programari")]
        public async Task<ActionResult<IEnumerable<Programare>>> GetProgramari()
        {
            return await _context.Programari.ToListAsync();
        }

        // GET: api/Programari/5
        [HttpGet]
        [Authorize(Roles = "Medic, Client")]
        [Route("api/Programari/{id}")]
        public async Task<ActionResult<Programare>> GetProgramare(int id)
        {
            var programare = await _context.Programari.FindAsync(id);

            if (programare == null)
            {
                return NotFound();
            }

            return programare;
        }

        [HttpGet]
        [Authorize(Roles = "Medic, Client")]
        [Route("programari/clientId={id}")]
        public async Task<ActionResult<List<Programare>>> GetProgramariByClientId(int id)
        {
            try
            {
                var programari = new List<Programare>();
                programari = await _context.Programari.ToListAsync();
                if (programari.Any())
                {
                    programari = programari.Where(programare => programare.ClientId == id).ToList();

                }

                return programari;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }

        [HttpGet]
        [Authorize(Roles = "Medic")]
        [Route("medicId={id}")]
        public async Task<ActionResult<List<Programare_Calendar>>> GetProgramariByMedicId(int id)
        {
            try
            {
                var programari = new List<Programare>();
                programari = await _context.Programari.ToListAsync();
                if (programari.Any())
                {
                    programari = programari.Where(programare => programare.MedicId == id).ToList();
                }

                var programariCalendar = new List<Programare_Calendar>();
                if (programari.Any())
                {
                    foreach(var programare in programari)
                    {
                        var programareCalendar = new Programare_Calendar
                        {
                            ClientId = programare.ClientId,
                            ClientName = _context.Clients.Where(s=>s.Id == programare.ClientId).First().Name,
                            Date = programare.date,
                            Id = programare.Id,
                            MedicId = programare.MedicId
                        };

                        programariCalendar.Add(programareCalendar);
                    }
                }

                return programariCalendar;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpGet]
        [Authorize(Roles = "Medic")]
        [Route("programariByDay/medicId={id}")]
        public async Task<ActionResult<Dictionary<DateTime, List<Programare>>>> GetProgramaryForEachDayByMedicId(int id)
        {
            try
            {
                var programari = await _context.Programari.ToListAsync();
                var programaryByDay = new Dictionary<DateTime, List<Programare>>();

                var distinctDates = programari.Select(s => s.date).Distinct().ToList();

                foreach(var date in distinctDates)
                {
                    //find all appointments that are on this day; 
                    var listAppOnCurrentDay = new List<Programare>();
                    foreach(var app in programari)
                    {
                        if(DateTime.Compare(app.date, date) == 0 && app.MedicId == id)
                        {
                            listAppOnCurrentDay.Add(app);
                        }
                    }

                    programaryByDay.Add(date, listAppOnCurrentDay);
                }

                return programaryByDay;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        

        // POST: api/Programari
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Route("")]
        public async Task<ActionResult<Programare>> PostProgramare([FromBody]Programare programare)
        {
            try
            {
                _context.Programari.Add(programare);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetProgramare", new { id = programare.Id }, programare);
            }
            catch (DbUpdateException ex)
            {
                throw ex;
            }
            catch(Exception exx)
            {
                throw exx;
            }
            
        }

        // DELETE: api/Programari/5
        [HttpDelete]
        [Route("{id}")]
        public async Task<ActionResult<Programare>> DeleteProgramare(int id)
        {
            var programare = await _context.Programari.FindAsync(id);
            if (programare == null)
            {
                return NotFound();
            }

            _context.Programari.Remove(programare);
            await _context.SaveChangesAsync();

            return programare;
        }

        private bool ProgramareExists(int id)
        {
            return _context.Programari.Any(e => e.Id == id);
        }

        // PUT: api/Programari/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutProgramare(int id, Programare programare)
        //{
        //    if (id != programare.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(programare).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!ProgramareExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}
    }
}
