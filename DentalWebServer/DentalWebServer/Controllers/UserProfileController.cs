﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DentalWebServer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private UserManager<ApplicationUser> userManager;
        private readonly DentalWebContext _context;
        public UserProfileController(UserManager<ApplicationUser> userManager, DentalWebContext context)
        {
            this.userManager = userManager;
            this._context = context;
        }

        [HttpGet]
        [Route("Medic")]
        [Authorize(Roles = "Medic")]
        public async Task<object> GetMedicUserProfile()
        {
            string userId = User.Claims.First(x => x.Type == "UserID").Value;
            var user = await userManager.FindByIdAsync(userId);
            var medic = _context.Medics.Where(s => s.UserId == userId).FirstOrDefault();

            return new
            {
                Email = user.Email,
                UserName = user.UserName,
                MedicId = medic.Id,
                UserId = medic.UserId,
                Name = medic.Name
            };
        }

        [HttpGet]
        [Authorize]
        public async Task<object> GetUserProfile()
        {
            string userId = User.Claims.First(x => x.Type == "UserID").Value;
            var user = await userManager.FindByIdAsync(userId);
            var client = _context.Clients.Where(s => s.UserId == userId).FirstOrDefault();
            var programari = _context.Programari.Where(s => s.ClientId == client.Id).ToList();
            var serviciiVizate = _context.ServiciiClienti.Where(s => s.ClientId == client.Id).ToList();

            var serviciiVizateDTO = new List<Serviciu_DTO>();

            if (serviciiVizate.Any())
            {
                foreach (var serviciuVizat in serviciiVizate)
                {
                    var serviciuDTO = new Serviciu_DTO();
                    serviciuDTO.Id = serviciuVizat.Id;
                    serviciuDTO.Name = _context.ServiciiMedicale.Where(s => s.Id == serviciuVizat.ServiciuId).First().Name;
                    serviciuDTO.Price = _context.ServiciiMedicale.Where(s => s.Id == serviciuVizat.ServiciuId).First().Price;

                    serviciiVizateDTO.Add(serviciuDTO);
                }
            }

            var programariDTO = new List<Programare_DTO>();
            if (programari.Any())
            {
                foreach(var programare in programari)
                {
                    var programareDTO = new Programare_DTO();
                    programareDTO.Id = programare.Id;
                    programareDTO.ClientId = programare.ClientId;
                    programareDTO.Date = programare.date;
                    programareDTO.DoctorName = _context.Medics.Where(s => s.Id == programare.MedicId).First().Name;

                    programariDTO.Add(programareDTO);
                }
            }

            return new
            {
                Email = user.Email,
                UserName = user.UserName,
                FullName = client.Name,
                ClientId = client.Id,
                Programari = programariDTO,
                ServiciiVizate = serviciiVizateDTO
            };
        }

    }
}