﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using DentalWebServer.Models;
using DentalWebServer.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationSettings _appSettings;
        private readonly DentalWebContext _dentalContext;
        private Repository<Client> _clients;
        private Repository<Medic> _medics;
        private readonly AuthenticationContext _authContext;

        public ApplicationUserController(AuthenticationContext authContext, DentalWebContext context, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IOptions<ApplicationSettings> appSettings)
        {
            this._appSettings = appSettings.Value;
            this._userManager = userManager;
            this._signInManager = signInManager;
            this._dentalContext = context;
            this._clients = new Repository<Client>(this._dentalContext);
            this._medics = new Repository<Medic>(this._dentalContext);
            this._authContext = authContext;
        }

        [HttpPost]
        [Route("Register")]
        //POST: api/ApplicationUser/Register
        public async Task<Object> PostApplicationUser([FromBody]ApplicationUserModel model)
        {
            //inregistrez doar clienti, doctorii sunt inregistrati deja
            if(string.Equals(model.Role, "Client", StringComparison.InvariantCultureIgnoreCase))
            {
                var applicationUser = new ApplicationUser()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                };

                try
                {
                    var result = await _userManager.CreateAsync(applicationUser, model.Password);
                    await _userManager.AddToRoleAsync(applicationUser, model.Role);

                    //acum inserez si in tabela de client 
                    var beforeInsertedUserId = _authContext.Users.Where(user => user.UserName == model.UserName).ToList().FirstOrDefault().Id;
                    var newClient = new Client(model.FullName, beforeInsertedUserId);
                    _clients.Insert(newClient);
                    _clients.SaveChangesMethod();

                    return Ok(result);
                }
                catch (Exception ex)
                {

                    throw ex; 
                }
            } else
            {
                return BadRequest(new { message = "You cannot create an instance of Medic" });
            }
        }

        [HttpPost]
        [Route("Register/{specializare}")]
        public async Task<Object> PostApplicationUserMedic(string specializare, [FromBody]ApplicationUserModel model)
        {
                var applicationUser = new ApplicationUser()
                {
                    UserName = model.UserName,
                    Email = model.Email,
                };

                try
                {
                    var result = await _userManager.CreateAsync(applicationUser, model.Password);
                    await _userManager.AddToRoleAsync(applicationUser, model.Role);

                    //acum inserez si in tabela de Medic 
                    var beforeInsertedUserId = _authContext.Users.Where(user => user.UserName == model.UserName).ToList().FirstOrDefault().Id;
                    var newMedic = new Medic(model.FullName, specializare, beforeInsertedUserId);
                    _medics.Insert(newMedic);
                    _medics.SaveChangesMethod();

                    return Ok(result);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
        }

        [HttpPost]
        [Route("Login")]
        //POST: api/ApplicationUser/Login
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if(user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                //Get the role assigned to the user
                var role = await _userManager.GetRolesAsync(user);
                IdentityOptions _options = new IdentityOptions();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserID", user.Id.ToString()),
                        new Claim(_options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)), SecurityAlgorithms.HmacSha256)
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);

                return Ok(new { token = token });
            }
            else
            {
                return BadRequest(new { message = "Username or password is incorrect" });
            }
        }
    }
}