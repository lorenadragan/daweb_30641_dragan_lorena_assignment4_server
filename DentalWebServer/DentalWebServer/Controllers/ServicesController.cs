﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalWebServer.Models;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly DentalWebContext _context;

        public ServicesController(DentalWebContext context)
        {
            _context = context;
        }

        // GET: api/Services
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServiciuMedical>>> GetServiciiMedicale()
        {
            return await _context.ServiciiMedicale.ToListAsync();
        }

        // GET: api/Services/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServiciuMedical>> GetServiciuMedical(int id)
        {
            var serviciuMedical = await _context.ServiciiMedicale.FindAsync(id);

            if (serviciuMedical == null)
            {
                return NotFound();
            }

            return serviciuMedical;
        }

        // PUT: api/Services/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutServiciuMedical(int id, ServiciuMedical serviciuMedical)
        {
            if (id != serviciuMedical.Id)
            {
                return BadRequest();
            }

            _context.Entry(serviciuMedical).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiciuMedicalExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Services
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ServiciuMedical>> PostServiciuMedical(ServiciuMedical serviciuMedical)
        {
            _context.ServiciiMedicale.Add(serviciuMedical);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetServiciuMedical", new { id = serviciuMedical.Id }, serviciuMedical);
        }

        // DELETE: api/Services/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiciuMedical>> DeleteServiciuMedical(int id)
        {
            var serviciuMedical = await _context.ServiciiMedicale.FindAsync(id);
            if (serviciuMedical == null)
            {
                return NotFound();
            }

            _context.ServiciiMedicale.Remove(serviciuMedical);
            await _context.SaveChangesAsync();

            return serviciuMedical;
        }

        private bool ServiciuMedicalExists(int id)
        {
            return _context.ServiciiMedicale.Any(e => e.Id == id);
        }
    }
}
