﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalWebServer.Models;
using Microsoft.AspNetCore.Authorization;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedicsController : ControllerBase
    {
        private readonly DentalWebContext _context;

        public MedicsController(DentalWebContext context)
        {
            _context = context;
        }

        // GET: api/Medics
        [HttpGet]
        [Authorize(Roles = "Client, Medic")]
        public async Task<ActionResult<IEnumerable<Medic>>> GetMedics()
        {
            return await _context.Medics.ToListAsync();
        }

        // GET: api/Medics/5
        [HttpGet("{id}")]
        [Authorize(Roles = "Client, Medic")]
        public async Task<ActionResult<Medic>> GetMedic(int id)
        {
            var medic = await _context.Medics.FindAsync(id);

            if (medic == null)
            {
                return NotFound();
            }

            return medic;
        }

        //GET by name: api/Medics/Tracy Jones
        [HttpGet]
        [Route("getByName={name}")]
        [Authorize(Roles = "Client, Medic")]
        public async Task<ActionResult<Medic>> GetMedic(string name)
        {
            var medics = await _context.Medics.ToListAsync();
            var medicByName = medics.Where(medic => medic.Name == name).FirstOrDefault();

            if(medicByName == null)
            {
                return NotFound();
            }

            return medicByName;
        }

        // PUT: api/Medics/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles = "Medic")]
        public async Task<IActionResult> PutMedic(int id, Medic medic)
        {
            if (id != medic.Id)
            {
                return BadRequest();
            }

            _context.Entry(medic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medics
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(Roles = "Medic")]
        public async Task<ActionResult<Medic>> PostMedic(Medic medic)
        {
            _context.Medics.Add(medic);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedic", new { id = medic.Id }, medic);
        }

        // DELETE: api/Medics/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "Medic")]
        public async Task<ActionResult<Medic>> DeleteMedic(int id)
        {
            var medic = await _context.Medics.FindAsync(id);
            if (medic == null)
            {
                return NotFound();
            }

            _context.Medics.Remove(medic);
            await _context.SaveChangesAsync();

            return medic;
        }

        private bool MedicExists(int id)
        {
            return _context.Medics.Any(e => e.Id == id);
        }
    }
}
