﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DentalWebServer.Models;

namespace DentalWebServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserServicesController : ControllerBase
    {
        private readonly DentalWebContext _context;

        public UserServicesController(DentalWebContext context)
        {
            _context = context;
        }

        // GET: api/UserServices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ServiciiClienti>>> GetServiciiClienti()
        {
            return await _context.ServiciiClienti.ToListAsync();
        }

        // GET: api/UserServices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServiciiClienti>> GetServiciiClienti(int id)
        {
            var serviciiClienti = await _context.ServiciiClienti.FindAsync(id);

            if (serviciiClienti == null)
            {
                return NotFound();
            }

            return serviciiClienti;
        }

        //GET by client ID
        [HttpGet]
        [Route("clientId={id}")]
        public async Task<ActionResult<List<ServiciiClienti>>> GetServiciiClientiByCliendId(int id)
        {
            var serviciiClienti = await _context.ServiciiClienti.ToListAsync();
            serviciiClienti = serviciiClienti.Where(s => s.ClientId == id).ToList();

            return serviciiClienti;
        }


        // PUT: api/UserServices/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutServiciiClienti(int id, ServiciiClienti serviciiClienti)
        {
            if (id != serviciiClienti.Id)
            {
                return BadRequest();
            }

            _context.Entry(serviciiClienti).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ServiciiClientiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        

        // POST: api/UserServices
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ServiciiClienti>> PostServiciiClienti(ServiciiClienti serviciiClienti)
        {
            _context.ServiciiClienti.Add(serviciiClienti);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetServiciiClienti", new { id = serviciiClienti.Id }, serviciiClienti);
        }

        // DELETE: api/UserServices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ServiciiClienti>> DeleteServiciiClienti(int id)
        {
            var serviciiClienti = await _context.ServiciiClienti.FindAsync(id);
            if (serviciiClienti == null)
            {
                return NotFound();
            }

            _context.ServiciiClienti.Remove(serviciiClienti);
            await _context.SaveChangesAsync();

            return serviciiClienti;
        }

        private bool ServiciiClientiExists(int id)
        {
            return _context.ServiciiClienti.Any(e => e.Id == id);
        }
    }
}
