﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class DentalWebContext : DbContext
    {
        public DentalWebContext(DbContextOptions<DentalWebContext> options) : base (options)
        {

        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Medic> Medics { get; set; }
        public DbSet<Programare> Programari { get; set; }
        public DbSet<ServiciiClienti> ServiciiClienti { get; set; }
        public DbSet<ServiciuMedical> ServiciiMedicale { get; set; }
    }
}
