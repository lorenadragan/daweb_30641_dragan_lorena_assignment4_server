﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DentalWebServer.Models
{
    public class AuthenticationContext: IdentityDbContext<ApplicationUser>
    {
        public AuthenticationContext(DbContextOptions options) : base(options)
        {

        }
    }
}
