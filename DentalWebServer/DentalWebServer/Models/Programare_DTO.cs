﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class Programare_DTO
    {
        public int Id{ get; set; }
        public int ClientId { get; set; }
        public string DoctorName { get; set; }
        public DateTime Date { get; set; }
    }
}
