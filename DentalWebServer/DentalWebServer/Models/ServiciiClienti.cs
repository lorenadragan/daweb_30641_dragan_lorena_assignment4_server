﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class ServiciiClienti: IPersistableModel
    {
        [Key]
        public int Id { get; set; }
        
        [Column(TypeName = "int")]
        public int ClientId { get; set; }

        [Column(TypeName = "int")]
        public int ServiciuId { get; set; }
    }
}
