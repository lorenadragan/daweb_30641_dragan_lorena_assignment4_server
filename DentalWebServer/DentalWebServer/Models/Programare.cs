﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class Programare: IPersistableModel
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "int")]
        public int MedicId { get; set; }

        [Column(TypeName="smalldatetime")]
        public DateTime date { get; set; }

        [Column(TypeName = "int")]
        public int ClientId { get; set; }
    }
}
