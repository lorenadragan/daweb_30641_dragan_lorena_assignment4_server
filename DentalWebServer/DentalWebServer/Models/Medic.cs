﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class Medic: IPersistableModel
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(450)")]
        public string UserId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Name { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Specializare { get; set; }

        public Medic() { }

        public Medic(string name, string specializare, string userId)
        {
            this.Name = name;
            this.Specializare = specializare;
            this.UserId = userId;
        }
    }
}
