﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class Serviciu_DTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public double Price { get; set; }
    }
}
