﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Models
{
    public class Programare_Calendar
    {
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public DateTime Date { get; set; }
        public int Id { get; set; }
        public int MedicId { get; set; }
    }
}
