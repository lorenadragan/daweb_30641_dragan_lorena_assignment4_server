﻿using DentalWebServer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;

namespace DentalWebServer.Repository
{
    public interface ISave<TModel>
         where TModel : class, IPersistableModel
    {
        void SaveChangesMethod();

        void OnSaveValidateErrors(DbEntityValidationException dbEx);
    }
}
